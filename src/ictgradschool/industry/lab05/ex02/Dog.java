package ictgradschool.industry.lab05.ex02;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog implements IAnimal{
    private String name;
    private boolean is_it_Mammal;
    private int numleg;
    public Dog(String name, boolean is_it_Mammal, int numleg){
        this.name=name;
        this.is_it_Mammal=is_it_Mammal;
        this.numleg=numleg;
    }
    @Override
    public String sayHello() {
        return myName()+" the dog says woof woof.";
    }

    @Override
    public boolean isMammal() {
        return is_it_Mammal;
    }

    @Override
    public String myName() {
        return name;
    }

    @Override
    public int legCount() {
        return numleg;
    }
}
