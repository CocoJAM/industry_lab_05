package ictgradschool.industry.lab05.ex02;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Bird("Tweety", false, 2);
        animals[1] = new Dog("Bruno", true, 4);
        animals[2] = new Horse("Mr. Ed", true, 4, "PharLap");

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.
        // TODO If the animal also implements IFamous, print out that corresponding info too.


        for (int c= 0 ; c< list.length; c++){
            System.out.println(list[c].sayHello());
            if (list[c].isMammal()){
                System.out.println(list[c].myName()+" the "+ list[c].getClass().getSimpleName().toLowerCase()+ " is a mammal.");
            }
            else{
                System.out.println(list[c].myName()+" the "+list[c].getClass().getSimpleName().toLowerCase() + " is a non-mammal.");
            }
            System.out.println("Did I forget to tell you that I have "+ list[c].legCount()+" legs.");
            if (list[c] instanceof IFamous){
                IFamous r = (IFamous) list[c];
                System.out.println("This is a famous name of my animal type: " + r.famous());
            }
        }

    }

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
