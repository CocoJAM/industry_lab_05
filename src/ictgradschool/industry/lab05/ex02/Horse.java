package ictgradschool.industry.lab05.ex02;

/**
 * Represents a horse.
 *
 * TODO Make this implement IAnimal and IFamous, and provide appropriate implementations of those methods.
 */
public class Horse implements IAnimal,IFamous{
    private String name;
    private boolean is_it_Mammal;
    private int numleg;
    private String famous;

    public Horse(String name, boolean is_it_Mammal, int numleg,String famous){
        this.name=name;
        this.is_it_Mammal=is_it_Mammal;
        this.numleg=numleg;
        this.famous= famous;
    }
    @Override
    public String sayHello() {
        return myName()+" the horse says neigh.";
    }

    @Override
    public boolean isMammal() {
        return is_it_Mammal;
    }

    @Override
    public String myName() {
        return name;
    }

    @Override
    public int legCount() {
        return numleg;
    }

    @Override
    public String famous(){return famous;}
}
