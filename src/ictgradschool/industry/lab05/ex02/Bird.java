package ictgradschool.industry.lab05.ex02;

/**
 * Represents a Bird.
 *
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird implements IAnimal {
    private String name;
    private boolean is_it_Mammal;
    private int numleg;
    public Bird(String name, boolean is_it_Mammal, int numleg){
        this.name=name;
        this.is_it_Mammal=is_it_Mammal;
        this.numleg=numleg;
    }
    @Override
    public String sayHello() {
        return myName()+" the bird says tweet tweet.";
    }

    @Override
    public boolean isMammal() {
        return is_it_Mammal;
    }

    @Override
    public String myName() {
        return name;
    }

    @Override
    public int legCount() {
        return numleg;
    }
}
