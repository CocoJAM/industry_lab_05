package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public interface Animal {
	public void feed();
	public int costToFeed();
	public String getName();
	public int getValue();




}
