package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public class Cow extends Animal_1 implements ProductionAnimals{

	/** All cow instances will have the same, shared, name: "Cow" */


	public Cow()  {
		value = 1000;
		name = "Cow";
	}
	@Override
	public void feed() {
		if (value < 1500) {
			value += 100;
		}
	}
	@Override
	public int costToFeed() {
		return 60;
	}
	@Override
	public String getName() {
		return name;
	}
	@Override
	public int getValue() {
		return value;
	}
	@Override
	public String toString() {
		return name + " - $" + value;
	}
	@Override
	public void tick(){
		ticker_cow+=1;
		System.out.println("Cow ticker "+ticker_cow);

	}
	@Override
	public boolean harvestable(){
		if (ticker_cow >=10){
			ticker_cow=0;
			return true;
		}
		else{
			return false;
		}
	}
	@Override
	public int harvest() {
		while (harvestable()) {
			return 20;
		}
		return 0;
	}
}
