package ictgradschool.industry.lab05.ex04.farmmanager;

import ictgradschool.industry.lab05.ex04.farmmanager.animals.*;

import java.util.List;

import java.util.ArrayList;

public class Farm {
    private ArrayList<Animal> animals;
    private final int startingMoney;
    private int money;

    public Farm(int money) {
        this.money = money;
        this.startingMoney = money;

        // An ArrayList is like an array, except it can hold any number of elements.
        // We will properly learn how to use lists and other collections in a later lecture.
        this.animals = new ArrayList<>();
    }

    public Farm() {
        this(10000);
    }

    /**
     * Returns how much money the farm currently has.
     */
    public int getMoney() {
        return this.money;
    }

    /**
     * Returns how much money the farm started with.
     */
    public int getStartingMoney() {
        return this.startingMoney;
    }

    /**
     * Purchases an animal and bills the farm for it.
     *
     * @param animalName The name of the type of animal you wish to buy.
     * @return true if the animal can be bought, false otherwise.
     */
    public boolean buyAnimal(String animalName) {
        Animal_1 newAnimal;
        switch (animalName.toLowerCase()) {
            case "cow":
                newAnimal = new Cow();
                break;
            case "chicken":
                newAnimal = new Chicken();
                break;
            default:
                // Animal not recognized.
                return false;
        }

        int price = (int) (newAnimal.getValue() * 1.15);

        if (money >= price) {
            money -= price;
        } else {

            // Insufficient funds.
            return false;
        }

        animals.add(newAnimal);
        return true;
    }

    /**
     * Sells all of the stock on the farm.
     */
    public int sell() {
        // Calculate the value of all of your animals
        int totalPrice = 0;

        // The size() method of an ArrayList is the same as the length field of an Array.
        for (int i = 0; i < animals.size(); i++) {

            // Calling the get method on an ArrayList is the same as using the square braces on an Array.
            // For example, if animals were an Array, the next line would look like this:
            // Animal a = animals[i];
            Animal a = animals.get(i);
            totalPrice += a.getValue();
        }

        // Remove the animals from your farm and add their value to your money.
        // The clear() method of an ArrayList removes all items from it.
        this.animals.clear();
        money += totalPrice;
        return totalPrice;
    }

    /**
     * Go through each animal and if you have enough money to feed it,
     * subtract the cost to feed from your money and call the feed method on the animal.
     */
    public void feedAll() {
        int total_cost_to_feed = 0;
        for (int i = 0; i < animals.size(); i++) {
            total_cost_to_feed += animals.get(i).costToFeed();
            animals.get(i).feed();
        }
        if (this.money >= total_cost_to_feed) {
            this.money -= total_cost_to_feed;
        }

    }

    /**
     * Go through each animal and if it is of the type specified and you have enough money to feed it,
     * subtract the cost to feed from your money and call the feed method on the animal.
     */
    public void feed(String animalType) {

        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i).getClass().getSimpleName().toLowerCase().equals(animalType) && money >= animals.get(i).costToFeed()) {
                this.money -= (animals.get(i).costToFeed());
                animals.get(i).feed();
            }
        }
    }

    /**
     * Prints out a list of all of the stock on the farm.
     * If there are no animals in stock inform the user.
     */
    public void printStock() {
        if (!animals.isEmpty()) {
            System.out.println(animals);
        } else {
            System.out.println("There is no animals in stock.");
        }
    }

    // the tick method but it double ticks when there is two of the same
    public void tick() {
        if (!animals.isEmpty()) {
            for (int i = 0; i < animals.size(); i++) {
                if (animals.get(i) instanceof Cow || animals.get(i) instanceof Chicken) {
                    Animal_1 c =(Animal_1) animals.get(i);
                     c.tick();
                }
            }
        }
    }

    public void harvest() {
        for (int i = 0; i < animals.size(); i++) {
            if (animals.get(i) instanceof ProductionAnimals) {
                ProductionAnimals c =(ProductionAnimals) animals.get(i);
                    money += c.harvest();
            }
        }
    }

}
